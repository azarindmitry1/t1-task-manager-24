package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-remove-by-index";

    @NotNull
    public final static String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        @NotNull final String userId = getUserId();
        serviceLocator.getTaskService().removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
